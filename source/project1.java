package Project1;

import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Scanner;

public class project1 {
	
	double starttime;
	double endtime;
	int steps;
	nuclide[] nuclides;
	systemdata[] data;
	systemdata[] datanext;
	double deltaT;
	double deltaTnext;
	double[] analyticmax;
	int n;
	boolean isPartA;
	boolean isPartB;
	double[][] maxes;
	double[][] maxesnext;
	double maxerr[];
	
	public static void main(String[] args){
		new project1();
	}
	
	public project1(){
		isPartA = false;
		/*--------------------------------------------------------------------------------------------------------------------
		 * --------------------------------------------------------------------------------------------------------------------
		 * End Init Values
		 * --------------------------------------------------------------------------------------------------------------------
		 * Start Get Info
		 * --------------------------------------------------------------------------------------------------------------------
		 */
		n=0;
		Scanner reader = new Scanner(System.in);  // Reading from System.in
		while(n<=0){
			System.out.println("Please Enter the number of nuclides:");
			n = Integer.valueOf(reader.next()); // Scans the next token of the input as an int.
		}
		nuclides = new nuclide[n];
		int i = 0;
		while (i<n){
			nuclides[i]= new nuclide();
			i=i+1;
		}
		i = 0;
		while(i < n){
			System.out.println("What is the half life (in seconds) of nuclide " + (i+1) + "?");
			nuclides[i].setT12(Double.valueOf(reader.next()));
			System.out.println("What is the initial value of nuclide " + (i+1) + "?");
			nuclides[i].setNumber(Double.valueOf(reader.next()));
			i = i + 1;
		}
		i=0;
		System.out.println("What is the starting time? (in seconds)");
		starttime = Double.valueOf(reader.next());
		System.out.println("What is the ending time? (in seconds)");
		endtime = Double.valueOf(reader.next());
		System.out.println("What is the number of timesteps?");
		steps = Integer.valueOf(reader.next());
		reader.close();
		consoleOutputSeparator();
		/*--------------------------------------------------------------------------------------------------------------------
		 * --------------------------------------------------------------------------------------------------------------------
		 * End Get Info
		 * --------------------------------------------------------------------------------------------------------------------
		 * Begin Calculation
		 * --------------------------------------------------------------------------------------------------------------------
		 */
		deltaT = (endtime-starttime)/steps;
		deltaTnext=(endtime-starttime)/(steps+8000);
		data = new systemdata[steps+1];
		datanext = new systemdata[steps+8001];
		// input starting data
		while(i<steps){
			data[i] = new systemdata(n);
			i=i+1;
		}
		i=0;
		while(i<steps+8000){
			datanext[i] = new systemdata(n);
			i=i+1;
		}
		data[0].setall(0, starttime, nuclides);
		datanext[0].setall(0, starttime, nuclides);
		
		//calculate all other data using matrix exponential
		i = 1;
		while(i<steps+1){
			systemdata addData = new systemdata(n);
			addData.setall(i,starttime +(deltaT*i),matrixExponential(i,deltaT,data));
			data[i]= addData;
			i=i+1;
		}
		i = 1;
		while(i<steps+8001){
			systemdata addData = new systemdata(n);
			addData.setall(i,starttime +(deltaTnext*i),matrixExponential(i,deltaTnext,datanext));
			datanext[i]= addData;
			i=i+1;
		}
		/*--------------------------------------------------------------------------------------------------------------------
		 * --------------------------------------------------------------------------------------------------------------------
		 * End Calculation
		 * --------------------------------------------------------------------------------------------------------------------
		 * Begin Output
		 * --------------------------------------------------------------------------------------------------------------------
		 */
		i=0;
		String path = project1.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		try {
			String decodedPath = URLDecoder.decode(path, "UTF-8");
			FileWriter w = new FileWriter(decodedPath + "/output.csv");
			w.append(data[0].getoutput());
			int k = 1;
			while(k<data.length){
				w.append("\n" + data[k].getoutput());
				k=k+1;
			}
			w.flush();
			w.close();
			
		} catch (UnsupportedEncodingException e) {
			System.out.println("Error: Output file path could not be decoded");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Error: Filewriter could not be opened");
		}
		System.out.println("If a program error has not been thrown, the data can be found in output.csv");
		System.out.println("Which has been saved in the same folder as this program's .jar file");
		consoleOutputSeparator();
		/*--------------------------------------------------------------------------------------------------------------------
		 * --------------------------------------------------------------------------------------------------------------------
		 * End Output
		 * --------------------------------------------------------------------------------------------------------------------
		 * Begin Finding Maximums and Error
		 * --------------------------------------------------------------------------------------------------------------------
		 */
		
		i=0;
		int j=0;
		// find max of each and time
		maxes = new double[2][n];
		maxesnext = new double[2][n];
		double max= 0.0;
		double maxtime = 0;
		while(j<n)
		{
			while(i<data.length){
				if(max<data[i].numberDensity[j]){
					max = data[i].numberDensity[j];
					maxtime = data[i].time;
				}
				else{
				}
				i=i+1;
			}
			maxes[0][j] = max;
			maxes[1][j] = maxtime;
			max=0.0;
			i=0;
			j=j+1;
		}
		j=0;
		while(j<n)
		{
			while(i<datanext.length){
				if(max<datanext[i].numberDensity[j]){
					max = datanext[i].numberDensity[j];
					maxtime = datanext[i].time;
				}
				else{
				}
				i=i+1;
			}
			maxesnext[0][j] = max;
			maxesnext[1][j] = maxtime;
			max=0.0;
			i=0;
			j=j+1;
		}
		//output maxes for each nuclide
		j=0;
		while(i<n){
			System.out.println("The Maximum Number Density of Nuclide " + (i+1) + " is " + maxes[0][i]);
			System.out.println("       Which occurs at t = " + maxes[1][i]);
			System.out.println(" ");
			i=i+1;
		}
		consoleOutputSeparator();
		//check if working on specific part
		analyticmax = new double[n];
		analyticmax[0] = maxes[0][0]; 
		partA();
		partB();
		//find error
	}
	/*--------------------------------------------------------------------------------------------------------------------
	 * --------------------------------------------------------------------------------------------------------------------
	 * End Main Constructor
	 * --------------------------------------------------------------------------------------------------------------------
	 * Begin Program Methods
	 * --------------------------------------------------------------------------------------------------------------------
	 */
	public nuclide[] matrixExponential(int thisstep, double dt, systemdata[] data2) {
		int prevstep=thisstep-1;
		systemdata prevdata = data2[prevstep];
		nuclide[] newnuclides= new nuclide[n];
		int counter = 0;
		while(counter<n){
			if(counter == 0){
				newnuclides[counter]=new nuclide(nuclides[counter].halflife,(prevdata.numberDensity[0])*((1-((nuclides[counter].decayConstant())*(dt)))));
				} //for first nuclide in chain
			else { 
				newnuclides[counter] = new nuclide(nuclides[counter].halflife,((prevdata.numberDensity[counter-1])*(nuclides[counter-1].decayConstant()*dt))+(prevdata.numberDensity[counter]*(1-(nuclides[counter].decayConstant()*dt))));//for all other nuclides
			}
			counter = counter+1;
		};
		return newnuclides;
	}
	
	private void consoleOutputSeparator(){
		System.out.println(" ");
		System.out.println("________________________________________________________________________");
		System.out.println(" ");
		System.out.println(" ");
	}
	
	private void partA(){
		isPartA = false;
		if(n==2){
			if(nuclides[0].halflife==0.7&&nuclides[1].halflife==2&&nuclides[0].number==1&&nuclides[1].number==0&&starttime==0.0&&endtime==5.0){
				isPartA = true;
			}
		}
		if(isPartA){
			System.out.println("You Appear to be working on Part A");
			analyticmax[1] = 0.5681960074; //according to analytic solution for part A (on paper)
			System.out.println("Your Error for Nuclide B is " + (analyticmax[1]-maxes[0][1]));
			consoleOutputSeparator();
		}
	}
	
	private void partB(){
		isPartB = false;
		if(n==5){
			if(nuclides[0].halflife==2&&nuclides[1].halflife==3&&nuclides[2].halflife==1.1&&nuclides[3].halflife==0.1&&nuclides[4].halflife==2&&nuclides[0].number==2&&nuclides[1].number==0&&nuclides[2].number==0&&nuclides[3].number==0&&nuclides[4].number==0&&starttime==0.0&&endtime==10.0){
				isPartB = true;
			}
		}
		if(isPartB){
			System.out.println("You Appear to be working on Part B");
			double[] pBError = new double[n];
			int iterator = 0;
			while(iterator<n){
				pBError[iterator] = maxes[0][iterator] - maxesnext[0][iterator];
				iterator=iterator+1;
			}
			iterator = 0;
			maxerr = new double[2];
			maxerr[0] = 0;
			maxerr[1] = 0;
			while(iterator<pBError.length){
				if(pBError[iterator]>maxerr[1]){
					maxerr[0] = pBError[iterator];
					maxerr[1] = iterator;
				}
				iterator=iterator+1;
			}
			System.out.println("Your Maximum error (between all nuclides) between our");
			System.out.println(" approximation and absolute is " + maxerr[0]);
			System.out.println("It should be noted that our \"absolute\" is actually");
			System.out.println(" another approximation made with a much greater amount of timesteps (+8000)");
			consoleOutputSeparator();
		}
	}
}