package Project1;

public class systemdata {
	int stepnumber;
	double time;
	double[] numberDensity;
	
	public systemdata(int n){
		numberDensity = new double[n];
	}
	
	public void setall(int s, double d, nuclide[] n){
		stepnumber = s;
		time = d;
		int i = 0;
		while(i<numberDensity.length){
			numberDensity[i]= n[i].number;
			i=i+1;
		}
	}
	
	public String getoutput(){
		StringBuilder sb = new StringBuilder();
		sb.append(String.valueOf(time));
		int counter = 0;
		while(counter<numberDensity.length){
			sb.append("," + String.valueOf(numberDensity[counter]));
			counter = counter+1;
		}
		String out = sb.toString();
		return out;
	}
}
