package Project1;

public class nuclide {
	
	public Double halflife;
	public double number;
	
	public nuclide() {
	}
	
	public nuclide (Double t, double d){
		halflife = t;
		number = d;
	}
	
	public void setT12(Double double1){
		halflife = double1;
	}
	
	public void setNumber(Double double1){
		number = double1;
	}
	
	public double getIV(){
		return number;
	}
	
	public Double getT12(){
		return halflife;
	}
	
	public double decayConstant(){
		return 0.6934718056/halflife;
	}
}
